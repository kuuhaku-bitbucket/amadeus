# AMADEUS_JAVASCRIPT-2

# 한국어

게임 슈타인즈 게이트에 나오는 AI 아마데우스를 컨셉으로 하는 음성비서 시스템

URL : https://amadeusjs.netlify.app/

사용 가능 커맨드

* 크리스티나 <br>
* 부탁해 <br>
* 헬로 <br>
* 무엇을 할 수 있어? <br>
* 타임머신은 가능할까?

# 日本語

ゲームシュタインズ・ゲートに出るAIアマデウスをコンセプトにする音声秘書システム

URL : https://amadeusjs.netlify.app/

使用可能コマンド

* クリスティーナ <br>
* お願い <br>
* ハロー <br>
* 何ができる <br>
* タイムマシンは可能かどうか
